# Mailu
![mailu](/uploads/7caf58c3cd0f1eca06873c9a8fb9e634/mailu.png)
    Mailu is a simple yet full-featured mail server as a set of Docker images. It is free software (both as in free beer and as in free speech), open to suggestions and external contributions. The project aims at providing people with an easily setup, easily maintained and full-featured mail server while not shipping proprietary software nor unrelated features often found in popular groupware.

![mailu-create](/uploads/1de3f12342b0df592de3fec804e35998/mailu-create.png)

     Main features include:
        Standard email server, IMAP and IMAP+, SMTP and Submission
        Advanced email features, aliases, domain aliases, custom routing
        Web access, multiple Webmails and administration interface
        User features, aliases, auto-reply, auto-forward, fetched accounts
        Admin features, global admins, announcements, per-domain delegation, quotas
        Security, enforced TLS, Letsencrypt!, outgoing DKIM, anti-virus scanner
        Antispam, auto-learn, greylisting, DMARC and SPF
        Freedom, all FOSS components, no tracker included
        
# 安装
    1、建子目录
        mkdir mailu
    2、克隆文件
        git clone ssh://git@gitlab.expresscare.cn:10022/linfang/mailu.git
    
    3、修改配置
    3.1 .env 文件
        -应该修改hostname，domain等；
        -secrect_key应该每次都新建16bytes，随机字符
            注意！使用旧的key会被rspamd拦截某些邮件
        -TLS_FLAVOR设为cert，自定义ca文件
        -MESSAGE_SIZE_LIMIT=209715200,200M附件大小
        -version:1.5。截至2019/01/06，其他latest/master版本管理命令不可用
        其他定义见https://mailu.io/compose/setup.html，https://mailu.io/configuration.html
    3.2 替换 certs目录中的ca文件内容，名字不变
    
    4、启动镜像
        docker-compose up -d
    
    5、创建管理用户
        docker-compose run --rm admin python manage.py admin {root} {example.net} {password}
        说明：  {root}--用户名，如postmaster
                {example.net}--域名，如3dmodel.cc
                {password}--密码，明文
        另外：本镜像有个导入其他邮箱用户密码的功能，user_import：
            docker-compose run --rm admin python manage.py user --hash_scheme='SHA512-CRYPT' myuser example.net '$6$51ebe0cb9f1dab48effa2a0ad8660cb489b445936b9ffd812a0b8f46bca66dd549fea530ce'
        详见：https://mailu.io/cli.html
    
    # 内存不够问题描述
        要求内存1.5G以上，当内存不够时，redis无法启动，需要配置memory cache和打开vm.overcommit_memory开关：
        - sysctl vm.overcommit_memory = 1

# 使用
    - 管理界面：
        https://{hostname.domain}/admin
        可设置用户quota，密码，服务栈的运行状态，rspamd可观察邮件过滤情况
    
    - web邮件客户端：
        https://{hostname.domain}/webmail
    
    - rainloop的admin界面(初始用户名=admin,psw=12345)：
        https://{hostname.domain}/webmail/?admin
        可设置背景，登录logo，页面等
        
    - webmail客户端端rainloop的设置：
        # 设置文件位置：webmail/_data_/_default_/application.ini，config.ini
        # To disable the "powered by" link:
          searching below file in the "mailu_webmail_1" container:
           /var/www/html/rainloop/v/1.12.1/app/libraries/RainLoop/Actions.php
          using nano to edit it
          Search for "powered" and change it to false.
         
        # or: To change the "powered by" link:
         For Admin Footer Editing, inside the "mailu_webmail_1" container
            /var/www/html/rainloop/v/1.12.1/app/templates/Views/Admin/AdminLogin.html
         For User Footer Editing,  inside the "mailu_webmail_1" container
            /var/www/html/rainloop/v/1.12.1/app/templates/Views/User/Login.html
         
         After finish the change ,make the change taking effect, in the host directory:
             webmail/_data_/_default_/application.ini
         disable template caching:
            [labs]
                cache_system_data = Off
        
    - 邮件过滤系统Rspamd的设置
        #官网：https://rspamd.com/doc/quickstart.html#introduction
        #关闭greylist模块,其他模块类似
            1 overrides/rspamd/增加greylist.conf:
            2 里面仅一行：enabled = false;
        
        #有webUI可以设置rspamd的配置规则：
        https://mail.expresscare.cn/admin/antispam/
         /var/lib/rspamd/dynamic，
         "actions": [
            {
                "name": "rewrite subject",
                "value": "999"
            }
        ],
        
# 子目录文件说明
    .env                标准的docker-compose参数设置文件
    certs/              认证文件
    docker-compose.yml  启动文件，标准的docker-compose
    data/               mailu的共用数据目录 
    mail/               用户邮箱数据
    dkim/               rspamd、admin都需要 的dkim
    filter/             antivirus,rspamd过滤器 
    overrides/          rspamd,smtp,imap的覆盖子目录
        rspamd/         rspamd专用的覆盖设置子目录,将相关的设置文件放入即可更新    
          greylist.conf enable=false; 关闭greylist模块
          actions.conf  {
                        reject = 999;
                        add_header = 999;
                        rewrite_subject = 999;
                        greylist = 999;
                        # Set rewrite subject to this value (%s is replaced by the original subject)
                        subject = "%s"  #用原字符串替代，等于没有执行rewrite
                        }
        dovecot/        imap服务的覆盖子目录
          dovecot.conf  ###############
                        # IMAP & POP
                        ###############
                        protocol imap {
                          mail_plugins = $mail_plugins imap_quota imap_sieve 
                        #增加imap同一个userid连接的并发数到40
                          mail_max_userip_connections = 40
                        }
                        #增加pop3同一个userid连接的并发数到20
                        protocol pop3 {
                           mail_max_userip_connections = 20
                        }
                        注释掉以免产生循环调用
                        #!include_try /overrides/dovecot.conf
    redis/              
    dav/                webdav网页地址本
    webmail/            网页客户端,mount入镜像里面的/data
        _data_/
          _default_/
            configs/config.ini  设置rainloop的配置文件，allow_admin_panel = On允许访问admin界面
        themes/         copy了rainloop1.12.1的themes,增加Expresscare子目录，存储Expresscare背景图片,这样可以在admin界面中设置
    
# 用户使用第三方邮箱客户端设置 
    
如果大家需要用第三方软件来收发公司邮箱的邮件，比如MAIL或者FOXMAIL等，可以使用以下配置。

收件服务器地址：

- POP 服务器地址：mail.expresscare.cn 端口110，SSL 加密端口995
或
- IMAP 服务器地址： mail.expresscare.cn 端口143，SSL 加密端口993

        关于outlook中设置相同。注意：在imap收件高级设置中，要在server中保留原件的话，记得勾选

发件服务器地址：

- SMTP 服务器地址：mail.expresscare.cn 端口25， SSL 加密端口587 or 465

        关于outlook的设置，选择ssl，端口号587